# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://G1tGud@bitbucket.org/G1tGud/stroboskop.git
git add jscolor.js
git rm nepotrebno.js
git commit -a -m "Priprava potrebnih JavaScript knjižnic"
git push --all
```

Naloga 6.2.3:
https://bitbucket.org/G1tGud/stroboskop/commits/57e8f186d33a96de2a546d6c2c721a1fc9e23f3d

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/G1tGud/stroboskop/commits/0ccec386ec82ef9bbfc1f139aed032eb699905b8

Naloga 6.3.2:
https://bitbucket.org/G1tGud/stroboskop/commits/530d59fe18e0e7a58d27b18eba4301f6bedb5d40

Naloga 6.3.3:
https://bitbucket.org/G1tGud/stroboskop/commits/db69ed26876b276b69801fdb36b0f7423433a7fd

Naloga 6.3.4:
https://bitbucket.org/G1tGud/stroboskop/commits/3bbe8f541f3fb59d9a82551cf6c858c926133e66

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push --all
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/G1tGud/stroboskop/commits/5a03506af52b54a609e2187c8b0b81b41afca5e2

Naloga 6.4.2:
https://bitbucket.org/G1tGud/stroboskop/commits/bbcc222dcd837f8c0009e8b104c54b2b2840e9be

Naloga 6.4.3:
https://bitbucket.org/G1tGud/stroboskop/commits/435eaafbbd22bbb3181253fc58cffcaac5852ca0

Naloga 6.4.4:
https://bitbucket.org/G1tGud/stroboskop/commits/1d33e5a506d5e7429b373944be7aa5921ec93c21
